# GEO_APP_DAS01

Jupyter Notebook for comparing seismic recordings obtained with a DAS interrogator and standard geophones


[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/npa-jnotebooks%2Fgeo_app_das01/master?filepath=UNIMIB_GEO_APP_DAS01.ipynb)
